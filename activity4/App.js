import React from 'react';
import 'react-native-gesture-handler'
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, 
  Text, 
  View,
  Button,
  Image,
 } from 'react-native';
 import { NavigationContainer, StackActions, TabActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import screen1 from './screens/screen1';
import screen2 from './screens/screen2';
import screen3 from './screens/screen3';
import screen4 from './screens/screen4';
import screen5 from './screens/screen5';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Text: {
    fontWeight: 'bold',
    height: 50,
  },
  
});

function Page1({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.container}>
        <Image
          style={{ height: 500, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/image1.png')}

        />
      </View>
      <Text style={styles.Text}>I chose IT because I want to connect with other people</Text>
      <Button 
        title="Continue"
        onPress={() => navigation.navigate('Page 2')}
      />
    </View>
  );
}
function Page2({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.container}>
        <Image
          style={{ height: 500, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/image2.png')}
          />
      </View>
      <Text style={styles.Text}>Because I am very fond of computers</Text>
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Page 3')}
      />
    </View>
  );
}
function Page3({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.container}>
        <Image
          style={{ width: 500, height: 500, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/image3.png')}
          />
      </View>
      <Text style={styles.Text}>Because I am a nomad when it comes to technology</Text>
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Page 4')}
      />
    </View>
  );
}
function Page4({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.container}>
        <Image
          style={{ width: 500, height: 500, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/image4.png')}
          />
      </View>
      <Text style={styles.Text}>Because I want to complete my bucket list</Text>
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Page 5')}
      />
    </View>
  );
}
function Page5({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={styles.container}>
        <Image
          style={{ height: 500, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/image5.png')}
          />
      </View>
      <Text style={styles.Text}>Because I want to build a better future</Text>
      <Button 
        title="Home"
        onPress={() => navigation.navigate('Page 1')}
      />
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Page 1" component={Page1} />
        <Stack.Screen name="Page 2" component={Page2} />
        <Stack.Screen name="Page 3" component={Page3} />
        <Stack.Screen name="Page 4" component={Page4} />
        <Stack.Screen name="Page 5" component={Page5} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

