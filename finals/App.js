import React from 'react';
import 'react-native-gesture-handler'
import { 
  Text, 
  View,
  Button,
  Image,
  ImageBackground, TouchableOpacity
 } from 'react-native';
 import { NavigationContainer,} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const image = require('./assets/menubg.jpg');
const image2= require('./assets/leaves.jpg');
const image3= require('./assets/watercolor.jpg');
const image4=require('./assets/brown.jpg');
const image5=require('./assets/grey.jpg');
const image6=require('./assets/splash2.jpg');
function SplashScreen({navigation}) {
  setTimeout (() => {
    navigation.replace('Home');

  }, 3000);
  return (
    <ImageBackground source={image6} resizeMode='cover' style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      </ImageBackground>
  );
}
function Page1({navigation}) {
  return (
    <ImageBackground source={image}  style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View >
        
      </View>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17, }}>Welcome To The Simple </Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17,}}>Recipe Application </Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 10,}}>By: Kristoffe Angelo P. Ambe </Text>
      <Text></Text>
      <Text style={{fontFamily: 'TimesNewRomanPS-ItalicMT', fontSize: 20,}}>Table of Contents: </Text>
      <Text></Text>
      <Text></Text>
      <Text style={{fontFamily: 'TimesNewRomanPS-ItalicMT', fontSize: 17}}>Page 1: Chicken Adobo Recipe </Text>
      <Text></Text>
      <Text></Text>
      <Text style={{fontFamily: 'TimesNewRomanPS-ItalicMT', fontSize: 17}}>Page 2: Chicken Tinola Recipe </Text>
      <Text></Text>
      <Text></Text>
      <Text style={{fontFamily: 'TimesNewRomanPS-ItalicMT', fontSize: 17}}>Page 3: Chicken Inasal Recipe </Text>
      <Text></Text>
      <Text></Text>
      <Text style={{fontFamily: 'TimesNewRomanPS-ItalicMT', fontSize: 17}}>Page 4: Fried Chicken Recipe </Text>
      <Button 
        title="Next Page"
        onPress={() => navigation.navigate('Chicken Adobo')}
      />
    </ImageBackground>
  );
}
function Page2({navigation}) {
  return (
    <ImageBackground source={image2} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View >
      <TouchableOpacity>
        <Image
          style={{ borderRadius: 40, width: 200, height: 200, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/Adobo.jpg')}
          />
          </TouchableOpacity>
      </View>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}>Ingredients Needed:</Text>
      <Text style={{fontFamily: 'Thonburi', paddingLeft: 10,paddingRight: 15, fontSize: 12 }}>2 tbsp canola oil - 6 cloves crushed garlic - pc sliced onion - 2 tbsp vinegar - 1/4 cup soy sauce - 1 cup water - 2 pcs bay leaves - 1 tsp crushed pepper - 2 pc chicken cubes - 1 tsp brown sugar</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> How to cook:</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 12}}>Time to cook: 30 minutes </Text>
      <Text style={{fontFamily:'Thonburi', paddingLeft: 16, fontSize: 13, paddingRight: 19}}>-Saute garlic and onions and sear chicken on all sides until browning is seen </Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi', fontSize: 13}}>  -Add vinegar, soy sauce, water, bay leaves and chicken cubes. Simmer for 10 minutes</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi', fontSize: 13}}>  -Fry chicken pieces in another pan until nicely browned.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi', fontSize: 13}}>  -Add chicken into sauce; add sugar and simmer for 10 minutes until sauce is thick.</Text>
      <Text> </Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> Serve Warm and Enjoy your Meal</Text>
      
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Chicken Tinola')}
      />
    </ImageBackground>
  );
}
function Page3({navigation}) {
  return (
    <ImageBackground source={image3} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View >
      <TouchableOpacity>
        <Image
          style={{ borderRadius: 40, width: 200, height: 200, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/tinola.jpg')}
          />
          </TouchableOpacity>
      </View>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}>Ingredients Needed:</Text>
      <Text style={{fontFamily: 'Thonburi', paddingLeft: 10,paddingRight: 15, fontSize: 12 }}>15ml cooking oil - 1 onion - 2 minced garlic - 1.4cm sliced ginger - 15ml fish sauce - 2 cans chicken broth - 1 chayote squash - salt - pepper - 1 boy choy - 230g spinach</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> How to cook:</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 12}}>Time to cook: 30 minutes </Text>
      <Text style={{fontFamily:'Thonburi', paddingLeft: 20, fontSize: 13, paddingRight: 29}}>-Add sliced onion, minced garlic and chicken. Stir fry.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:19.5, fontSize: 13, paddingRight: 20}}>-Next, add the sliced ginger, fish sauce, chicken. Cook for 5 mins.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:13, fontSize: 13,paddingRight: 20}}>-Then, pour the chicken broth into the pot and let it cook for 5 minutes.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:13, fontSize: 13,paddingRight: 20}}>-Add the chayote squash and let it simmer until the chicken is cooked, or for around 10 minutes.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:19, fontSize: 13,paddingRight: 20}}>-Season with salt and pepper to taste then finally add boy choy for 1 - 2 minutes</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> Serve Warm and Enjoy your Meal</Text>
      
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Chicken Inasal')}
      />
    </ImageBackground>
  );
}
function Page4({navigation}) {
  return (
    <ImageBackground source={image4} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View >
      <TouchableOpacity>
        <Image
          style={{ borderRadius: 40, width: 200, height: 200, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/inasal.jpg')}
          />
          </TouchableOpacity>
      </View>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}>Ingredients Needed:</Text>
      <Text style={{fontFamily: 'Thonburi', paddingLeft: 10,paddingRight: 15, fontSize: 12 }}>2 cloves minced garlic - 1 cup vinegar - 2 lbs chicken cut in pcs - 1/4 salt and pepper - 1 cup fish sauce - any barbecue sauce</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> How to cook:</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 12}}>Time to cook: 40 minutes </Text>
      <Text style={{fontFamily:'Thonburi', paddingLeft: 16, fontSize: 13, paddingRight: 10}}>-In a bowl, mix garlic, vinegar, salt and pepper, fish sauce and chicken.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:17, fontSize: 13, paddingRight: 29}}>-Shake the mixture. Marinade chicken for 1 - 3 hours.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:13, fontSize: 13,paddingRight: 20}}>-Grill the chicken and baste with barbecue sauce. Grill each chicken for 40 mins, 10 mins each side.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi', fontSize: 13,paddingRight: 75}}>-Transfer grilled chicken on a serving plate</Text>
      <Text> </Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> Enjoy Your Meal</Text>
      
      <Button
        title="Continue"
        onPress={() => navigation.navigate('Fried Chicken')}
      />
    </ImageBackground>
  );
}
function Page5({navigation}) {
  return (
    <ImageBackground source={image5} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View >
      <TouchableOpacity>
        <Image
          style={{ borderRadius: 40, width: 200, height: 200, justifyContent: 'center', alignItems: 'center'}}
          source={require('./assets/fried.jpg')}
          />
          </TouchableOpacity>
      </View>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}>Ingredients Needed:</Text>
      <Text style={{fontFamily: 'Thonburi', paddingLeft: 10,paddingRight: 15, fontSize: 12 }}>flour - paprika - chicken - salt - pepper - oil - buttermilk</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> How to cook:</Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 12}}>Time to cook: 40 minutes </Text>
      <Text style={{fontFamily:'Thonburi', paddingLeft: 16, fontSize: 13, paddingRight: 10}}>-Combine flour, paprika, salt and pepper in a bowl or zip-top bag.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:17, fontSize: 13, paddingRight: 29}}>-In a shallow bowl, soak chicken on buttermilk. Place chicken on the flour mixture and coat.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:21, fontSize: 13,paddingRight: 20}}>-Pour oil on skillet and boil to with high heat. Brown the chicken on both sides and cook for 30 minutes.</Text>
      <Text> </Text>
      <Text style={{fontFamily:'Thonburi',paddingLeft:8, fontSize: 13,paddingRight: 20}}>-Drain chicken on paper towel and serve in a plate or place of choice.</Text>
      <Text> </Text>
      <Text style={{fontFamily: 'Zapfino', fontSize: 17}}> Enjoy Your Meal</Text>
      
      <Button
        title="Home"
        onPress={() => navigation.navigate('Home')}
      />
    </ImageBackground>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
     <Stack.Navigator initialRouteName='SplashScreen'
  screenOptions={{
    headerShown: false
  }}
>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="Home" component={Page1} />
        <Stack.Screen name="Chicken Adobo" component={Page2} />
        <Stack.Screen name="Chicken Tinola" component={Page3} />
        <Stack.Screen name="Chicken Inasal" component={Page4} />
        <Stack.Screen name="Fried Chicken" component={Page5} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

