import React, { useEffect } from 'react';
import {View, StyleSheet, Text, Image,} from 'react-native';
import PushNotification from 'react-native-push-notification';

export default function splashscreen({ navigation }) {

    useEffect(() => {
        createChannels();
        setTimeout(() => {
            navigation.replace('My Tasks');
        }, 2000);
    }, []);

    const createChannels = () => {
        PushNotification.createChannel(
            {
                channelId: "task-channel",
                channelName: "Task Channel"
            }
        )
    }

    return (
        <View style={StyleSheet.body} >
            <Image
                style={StyleSheet.logo}
                source={require('../assets/it105.jpg')}
            />
            <Text>To Do List</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffc0cb',
    },
    logo: {
        width: 150,
        height: 150,
        margin: 20,
    },
    text: {
        fontSize: 40,
        color: '#ffffff',
    },
})