import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TextInput,ScrollView, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
export default function Todo({ navigation }) {
  const [getText, setText] = useState('');
  const [getList, setList] = useState([]);
  
  const addItem = () => {
    console.log(getText);
    setList([
        
      {key:Math.random().toString() 
        , data:getText},
        ...getList
        
      ]);
      
    setText('');
   
  }
  const removeItem = (itemKey) => {
    setList(list => getList.filter(item => item.key != itemKey));
  }
  return (
    <View style={styles.container}>
      <Text style={styles.title}></Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textInput}
          placeholder=""
          onChangeText={text => setText(text)}
          value={getText}
        />
        <Button
       
          title="Add Task"
          onPress={addItem}
/>
       

      </View>
      <ScrollView style={styles.scrollview}>
        {getList.map((item) => 
        <TouchableOpacity
          key={item.key}
          activeOpacity={0.5}
         
        >
        <View style={styles.scrollviewItem} >
          <Text style={styles.scrollviewText}>{item.data}</Text>
          <TouchableOpacity
           onPress={()=>removeItem(item.key)}>
          <View style={styles.checktextcontainer}>
            <Text style={styles.crosstext}>✓</Text>
          </View>
          </TouchableOpacity>
        </View>
        </TouchableOpacity>
        )}
      </ScrollView>
      
    </View>
  );
}

const styles = StyleSheet.create({
  
  checktextcontainer: {
    backgroundColor: 'grey',
    borderRadius: 50,
    padding: 5,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  crosstext: {
    fontSize: 20,
    color: 'red',
    fontWeight: "bold"
  },
  scrollviewText: {
    fontSize: 26,
    color: 'white'
  },
  scrollview: {
    paddingTop: 20,
    width: '100%'
  },
  scrollviewItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: 'pink',
    alignSelf: "center",
    padding: 10,
    margin: 5,
    width: '90%',
    borderRadius: 10
  },
  title: {
    fontSize: 64,
    color: 'lightgrey'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 40
  },
  inputContainer: {
    flexDirection: "column",
    width: '70%',
    justifyContent: "space-between",
    alignItems: "center"
  },
  textInput: {
    borderColor: 'pink',
    borderWidth: 2,
    borderBottomWidth: 2,
    width: '70%',
     borderRadius: 5,
    fontSize: 16,
    padding: 10,
  }
});