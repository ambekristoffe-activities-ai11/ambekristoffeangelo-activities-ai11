import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import splashscreen from './Screens/splashscreen';
import Completed from './Screens/Completed';
import Todo from './Screens/Todo';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 
const Tab = createBottomTabNavigator();

function HomePage() {
    return (
        <NavigationContainer>
      <Tab.Navigator
        screenOptions={
          ({ route }) => ({
            tabBarIcon: ({ focused, size, color }) => {
              
              if (route.name === 'Todo') {
                <FontAwesomeIcon icon="fa-solid fa-clipboard-list" />
                size = focused ? 25 : 20;
              } else if (route.name === 'Completed') {
                <FontAwesomeIcon icon="fa-solid fa-clipboard-check" />
                size = focused ? 25 : 20;
              }
             
            }
          })
        }
        tabBarOptions={{
          activeTintColor: '#0080ff',
          inactiveTintColor: '#777777',
          labelStyle: { fontSize: 15, fontWeight: 'bold' }
        }}
      >
        <Tab.Screen 
        name={'To do'} 
        component={Todo} />
        <Tab.Screen name={'Completed Tasks Go Here'} component={Completed} />
      </Tab.Navigator>
      </NavigationContainer>
    );
  }
  
  const RootStack = createStackNavigator();
  
  /*function App() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <RootStack.Navigator
            initialRouteName="splashscreen"
            screenOptions={{
              headerTitleAlign: 'center',
              headerStyle: {
                backgroundColor: '#0080ff'
              },
              headerTintColor: '#ffffff',
              headerTitleStyle: {
                fontSize: 25,
                fontWeight: 'bold'
              }
            }}
          >
            <RootStack.Screen
              name="splashscreen"
              component={Splash}
              options={{
                headerShown: false,
              }}
            />
            <RootStack.Screen
              name="My Tasks"
              component={HomeTabs}
            />
            <RootStack.Screen
              name="Task"
              component={Task}
            />
          </RootStack.Navigator>
        </NavigationContainer>
      </Provider>
    )
            }*/
    
  
  
  export default HomePage;
